// Use this presets array inside your presetHandler
const presets = require('./presets');

// Complete this function:
const presetHandler = (requestTypeStr, presetIdx, newPresetArray) => {

    let returnArray = [0];

    // index validity
    if((preset[presetIdx] === undefined && requestTypeStr === "GET") || (newPresetArray[presetIdx] === undefined && requestTypeStr === "PUT")){
        returnArray[0] = 404;
    } else if (requestTypeStr !== "GET" && requestTypeStr !== "PUT") {
        returnArray[0] = 400;
    } else {
        returnArray[0] = 200;
        if (requestTypeStr === "GET"){
            returnArray.push(presets[presetIdx]);
        } else{
            returnArray.push(newPresetArray[presetIdx]);
        }
    }

    return returnArray;

}

// Leave this line so that your presetHandler function can be used elsewhere:
module.exports = presetHandler;







