// Drum Arrays
let kicks = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
let snares = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
let hiHats = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
let rideCymbals = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

const toggleDrum = (drumType, drumIdx) =>{
    switch(drumType[drumIdx]){
        case true:
            drumType[drumIdx] = false;
            break;
        case false:
            drumType[drumIdx] = true;
            break;
    }

}

const clear = drumTypeStr => {
    switch(drumTypeStr){
        case "kicks":
            kicks = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
            break;
        case "snares":
            snares = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
            break;
        case "hiHats":
            hiHats = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
            break;
        case "rideCymbals":
            rideCymbals = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
            break;
    }
}

const invert = drumTypeStr => {
    switch(drumTypeStr){
        case "kicks":
            for (let i = 0; i < kicks.length; i++){
                toggleDrum(kicks, i);
            }
            break;
        case "snares":
            for (let i = 0; i < snares.length; i++){
                toggleDrum(snares, i);
            }
            break;
        case "hiHats":
            for (let i = 0; i < hiHats.length; i++){
                toggleDrum(hiHats, i);
            }
            break;
        case "rideCymbals":
            for (let i = 0; i < rideCymbals.length; i++){
                toggleDrum(rideCymbals, i);
            }
            break;        
    }
}












